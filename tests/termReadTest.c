/**
   This tests the terminal reader on unit 0.
   The terminal file "term0.in" should read as such:
   'test\n'
 **/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>

char buffer[P2_MAX_LINE];

int P3_Startup(void *arg) {
  USLOSS_Console("P3_Startup:\n");

  USLOSS_Console("Calling Sys_TermRead on unit 0\n");
  int output;
  Sys_TermRead(buffer, P2_MAX_LINE, 0, &output);

  USLOSS_Console("Read complete: buffer now holds '%s'\n", buffer);

  assert(0 == strcmp(buffer, "test\n"));

  USLOSS_Console("Test complete\n");

  return 0;
}

void setup(void) {
    // Do nothing.
    system("echo test > term0.in");
}

void cleanup(void) {

}