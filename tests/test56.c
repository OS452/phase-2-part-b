#include <string.h>
#include <stdlib.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>
#include <stdio.h>


int P4_Startup(void *arg);

int P3_Startup(void *arg) {
        int *mbox = (int*) malloc(sizeof(int));
	int x = 0;
        int* pid = &x;

        int pid2 = Sys_Spawn("P4_Startup", P4_Startup, mbox, 4 *  USLOSS_MIN_STACK, 4, pid);
        assert(pid2 == 0);

	int *child = (int*) malloc(sizeof(int));
	int *status = (int*) malloc(sizeof(int));
	Sys_Wait(child, status);
        assert(*child == *pid);
        assert(*status == 12);
        
        
        return 7;

}


int P4_Startup(void* arg) {
    Sys_DumpProcesses();
    return 12;
}

void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}
