#include <string.h>
#include <stdlib.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>
#include <stdio.h>


/*
 * This tests error and correct conditions for Sys_Terminate 
 * */

int P4_Startup(void *arg);

int P3_Startup(void *arg) {
        
        //This prints a kernel function error  
        P2_Terminate(9);
       
	int x = 12;
        int* pid = &x;

        char* argument = "HELLO";
        int result = Sys_Spawn("P4_Startup", P4_Startup, argument, 4 *  USLOSS_MIN_STACK, 4, pid);
        assert(result == 0);
                 
        int* status = &x;
        Sys_Wait(pid, status);
        assert(*status == 15); 
     
       return 7;
}

int P4_Startup(void* arg) {
    char* string = (char*) arg;
    assert(strcmp(string, "HELLO") == 0);
    Sys_Terminate(15);
    USLOSS_Console("This does not print\n");
    return 13;
}

void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}
