#include <string.h>
#include <stdlib.h>
#include <usloss.h>
#include <phase1.h>
#include <phase2.h>
#include <assert.h>
#include <libuser.h>
#include <stdio.h>


int P4_Startup(void *arg);

int P3_Startup(void *arg) {
    
	int x = 0;
        int* pid = &x;         
        

        int *mbox = (int*) malloc(sizeof(int));
        Sys_MboxCreate(10, 20, mbox);
        assert(*mbox > 0);


        int pid2 = Sys_Spawn("P4_Startup", P4_Startup, mbox, 4 *  USLOSS_MIN_STACK, 4, pid);
        assert(pid2 == 0);
        assert(*pid > 0 && *pid < P1_MAXPROC);

        int KK = 8;
        int* size = &KK;
        char* buffer = malloc(8);
        USLOSS_Console("Going to try to receive message\n");
        Sys_MboxReceive(*mbox,buffer, size);
        assert(*size == 7);
        USLOSS_Console("Received message\n");

        KK = 8;
        USLOSS_Console("Going to try to receive message 2\n");
        Sys_MboxReceive(*mbox,buffer, size);
        assert(*size == 8);
        USLOSS_Console("Successfully received message 2\n");

        free(mbox);
        free(buffer);       
	return 7;
}

int P4_Startup(void* arg) {
    int* mbox = (int*) arg;

    USLOSS_Console("Sending message 1\n");
    int Ok = 7;
    int* size = &Ok;
    Sys_MboxSend(*mbox, "Working", size);
    printf("Done sending 1\n");

    Ok = 15;
    size = &Ok;
    USLOSS_Console("Sending message 2\n");
    Sys_MboxSend(*mbox, "Working8912345", size);
    USLOSS_Console("Done sending 2\n");
    return 0;
}

void setup(void) {
    // Do nothing.
}

void cleanup(void) {
    // Do nothing.
}
